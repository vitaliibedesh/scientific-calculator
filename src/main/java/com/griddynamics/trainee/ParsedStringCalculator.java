package com.griddynamics.trainee;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.StringTokenizer;
import java.util.function.BiFunction;
import java.util.function.Function;

public class ParsedStringCalculator {

    // list of available operators
    private Map<Character, BiFunction<Double, Double, Double>> operatorMap =
            new HashMap<>() {
                {
                    put('-', (a, b) -> a - b);
                    put('+', (a, b) -> a + b);
                    put('*', (a, b) -> a * b);
                    put('/', (a, b) -> a / b);
                    put('%', (a, b) -> a % b);            // тестити
                    put('^', Math::pow);                 // тестити
                }
            };

    // list of available functions
    private Map<String, Function<Double, Double>> functionMap = new HashMap<>() {
        {
            put("sin", Math::sin);
            put("cos", Math::cos);
            put("tan", Math::tan);
            put("sqrt", Math::sqrt);
            put("log", Math::log);

        }
    };

    public double calculate(String sIn) throws Exception {
        Stack<Double> stack = new Stack<>();
        StringTokenizer st = new StringTokenizer(sIn.replaceAll(",", "")
                .replace("[", "").replaceAll("]", ""));
        calculateTokenParts(stack, st);
        if (stack.size() > 1) {
            throw new Exception("Количество операторов не соответствует количеству операндов");
        }
        return stack.pop();
    }

    private void calculateTokenParts(Stack<Double> stack, StringTokenizer st) throws Exception {
        String sTmp;
        double dB;
        double dA;
        while (st.hasMoreTokens()) {
            try {
                sTmp = st.nextToken().trim();
                if (1 == sTmp.length() && operatorMap.containsKey(sTmp.charAt(0))) {
                    if (stack.size() < 2) {
                        throw new Exception("Неверное количество данных в стеке для операции " + sTmp);
                    }
                    dB = stack.pop();
                    dA = stack.pop();

                    if (operatorMap.containsKey(sTmp.charAt(0))) {
                        dA = operatorMap.get(sTmp.charAt(0)).apply(dA, dB);
                    } else {
                        throw new Exception("Недопустимая операция " + sTmp);
                    }
                    stack.push(dA);
                } else if (functionMap.containsKey(sTmp)) {
                    dA = stack.pop();
                    dA = functionMap.get(sTmp).apply(dA);
                    stack.push(dA);
                } else {
                    dA = Double.parseDouble(sTmp);
                    stack.push(dA);
                }
            } catch (Exception e) {
                throw new Exception("Недопустимый символ в выражении");
            }
        }
    }

    public Map<Character, BiFunction<Double, Double, Double>> getOperatorMap() {
        return operatorMap;
    }

    public Map<String, Function<Double, Double>> getFunctionMap() {
        return functionMap;
    }
}
