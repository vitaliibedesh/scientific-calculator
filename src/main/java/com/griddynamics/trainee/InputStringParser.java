package com.griddynamics.trainee;

import java.text.ParseException;
import java.util.Stack;
import java.util.StringTokenizer;

public class InputStringParser {

    ParsedStringCalculator parsedStringCalculator = new ParsedStringCalculator();

    // list of available operators
    private final String OPERATORS = "+-*/%^";

    // separator of arguments
    private final String SEPARATOR = ",";

    // temporary stack that holds operators, functions and brackets
    private Stack<String> stackOperations = new Stack<>();

    // stack for holding expression converted to reversed polish notation
    private Stack<String> stackRPN = new Stack<>();

    private boolean isNumber(String token) {
        try {
            Double.parseDouble(token);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private boolean isFunction(String token) {
        return parsedStringCalculator.getFunctionMap().containsKey(token);
    }

    private boolean isSeparator(String token) {
        return token.equals(SEPARATOR);
    }

    private boolean isOpenBracket(String token) {
        return token.equals("(");
    }

    private boolean isCloseBracket(String token) {
        return token.equals(")");
    }

    private boolean isOperator(String token) {
        return parsedStringCalculator.getOperatorMap().containsKey(token.charAt(0));
    }

    private byte getPrecedence(String token) {
        if (token.equals("+") || token.equals("-")) {
            return 1;
        }
        return 2;
    }

    public Double parseInputString(String expression) throws ParseException, Exception {
        // cleaning stacks
        stackOperations.clear();
        stackRPN.clear();

        // make some preparations
        expression = expression.replace(" ", "").replace("(-", "(0-")
                .replace(",-", ",0-");
        if (expression.charAt(0) == '-') {
            expression = "0" + expression;
        }
        // splitting input string into tokens
        StringTokenizer stringTokenizer = new StringTokenizer(expression,
                OPERATORS + SEPARATOR + "()", true);
        orderTokens(stringTokenizer);

        while (!stackOperations.empty()) {
            stackRPN.push(stackOperations.pop());
        }
        return parsedStringCalculator.calculate(stackRPN.toString());
    }

    private void orderTokens(StringTokenizer stringTokenizer) {
        // loop for handling each token - shunting-yard algorithm
        while (stringTokenizer.hasMoreTokens()) {
            String token = stringTokenizer.nextToken();
            if (isSeparator(token)) {
                while (!stackOperations.empty() && !isOpenBracket(stackOperations.lastElement())) {
                    stackRPN.push(stackOperations.pop());
                }
            } else if (isOpenBracket(token)) {
                stackOperations.push(token);
            } else if (isCloseBracket(token)) {
                while (!stackOperations.empty() && !isOpenBracket(stackOperations.lastElement())) {
                    stackRPN.push(stackOperations.pop());
                }
                stackOperations.pop();
                if (!stackOperations.empty() && isFunction(stackOperations.lastElement())) {
                    stackRPN.push(stackOperations.pop());
                }
            } else if (isNumber(token)) {
                stackRPN.push(token);
            } else if (isOperator(token)) {
                while (!stackOperations.empty() && isOperator(stackOperations.lastElement()) &&
                        getPrecedence(token) <= getPrecedence(stackOperations.lastElement())) {
                    stackRPN.push(stackOperations.pop());
                }
                stackOperations.push(token);
            } else if (isFunction(token)) {
                stackOperations.push(token);
            }
        }
    }

}
