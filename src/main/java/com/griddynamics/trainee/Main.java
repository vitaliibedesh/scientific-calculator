package com.griddynamics.trainee;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        InputStringParser inputStringParser = new InputStringParser();
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Please, type mathematical formula or if you want to leave type \"q\"");
            String input = scanner.nextLine();
            if (input.equals("q")) {
                scanner.close();
                return;
            }
            try {
                System.out.println(inputStringParser.parseInputString(input));
//                System.out.println(inputStringParser.parseInputString("3 * (( 4 / 5 + cos(10) - sin(50) * 2.35435 ) + 3) / 2"));
            } catch (Exception e) {
                System.out.println("Input string has bug, check it please and try one more time");
            }
        }
    }
}
